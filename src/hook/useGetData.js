import { useEffect, useState } from "react";

export const useGetData = () => {
  const [data, setData] = useState([]);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  let init = {
    method: "GET",
    headers: new Headers(),
    mode: "cors",
    cache: "default",
  };
  useEffect(() => {
    fetch("https://dog.ceo/api/breeds/image/random", init)
      .then((response) => response.json())
      .then((data) => setData([data]));
  }, []);
  return data;
};
