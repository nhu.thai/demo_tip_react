import { useEffect, useState } from "react";
import axios from "axios";
export const useGetDataPromiseAllDemo = () => {
  const [dataPrmFirst, setDataPrmFirst] = useState();
  const [dataPrmSecond, setDataPrmSecond] = useState();
  useEffect(() => {
    void (async () => {
      const [data1, data2] = await Promise.all([
        axios.get("https://api.github.com/repos/tannerlinsley/react-query"),
        axios.get("https://jsonplaceholder.typicode.com/posts"),
      ]);
      setDataPrmFirst(data1.data);
      setDataPrmSecond(data2.data);
    })();
  }, []);
  return { dataPrmFirst, dataPrmSecond };
};
