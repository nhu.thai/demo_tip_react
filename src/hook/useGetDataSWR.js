import useSWR from "swr";
import axios from "axios";

export const useGetDataSWR = () => {
  const { status, isLoading, error, data, refetch } = useSWR(
    "swrData",
    async () =>
      axios
        .get("https://jsonplaceholder.typicode.com/posts")
        .then((res) => res.data)
  );

  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;
  return { data, isLoading, error, status, refetch };
};
