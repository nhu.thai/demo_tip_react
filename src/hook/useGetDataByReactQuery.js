import axios from "axios";
import { useQuery } from "react-query";

export const useGetDataByReactQuery = () => {
  const { status, isLoading, error, data, isFetching, refetch } = useQuery({
    queryKey: ["repoData"],
    queryFn: () =>
      axios
        .get("https://jsonplaceholder.typicode.com/posts")
        .then((res) => res.data),
  });

  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;
  return { data, isLoading, error, isFetching, status, refetch };
};
