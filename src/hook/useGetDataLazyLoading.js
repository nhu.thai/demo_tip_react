import { useEffect, useState } from "react";

export const useGetDataLazyLoading = () => {
  const [data, setData] = useState([]);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  let init = {
    method: "GET",
    headers: new Headers(),
    mode: "cors",
    cache: "default",
  };
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts", init)
      .then((response) => response.json())
      .then((data) => setData(data));
  }, []);
  return data;
};
