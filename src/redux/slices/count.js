import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  name: 0,
};

const CountSice = createSlice({
  name: "refresh",
  initialState,
  reducers: {
    setCount: (state) => {
      state.count = state.count + 1;
    },
  },
});
export const { setName } = CountSice.actions;

export default CountSice.reducer;
