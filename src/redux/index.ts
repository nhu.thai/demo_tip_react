import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  refreshNotification: false,
};

const RefreshSlice = createSlice({
  name: "refresh",
  initialState,
  reducers: {
    setRefreshNotification: (state) => {
      state.refreshNotification = !state.refreshNotification;
    },
  },
});
export const { setRefreshNotification } = RefreshSlice.actions;

export default RefreshSlice.reducer;
