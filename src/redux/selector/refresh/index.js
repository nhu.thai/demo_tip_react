import { createSelector } from "reselect";

// *** đây là 'selector' của mình
const selectGetUsersOptions = (state) => state.getUsersOptions;
const selectGetUsers = (state) => state.users;

const getUsersSelector = createSelector(
  [selectGetUsers, selectGetUsersOptions],
  (users, getUsersOptions) => ({ users, getUsersOptions })
);

export default getUsersSelector;
