import { LoadingOutlined } from "@ant-design/icons";
import { useGetData } from "../hook/useGetData";
import { Spin } from "antd";
const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const withLoading = (WrappedComponent) => () => {
  const data = useGetData();
  console.log(data.length);
  return data.length === 0 ? (
    <Spin indicator={antIcon} />
  ) : (
    // <div>loading</div>
    <WrappedComponent url={data[0].message} />
  );
};
export default withLoading;
