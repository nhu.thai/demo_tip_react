import { useGetDataSWR } from "../hook/useGetDataSWR";
import { Post } from "./LazyLoadingDemo";

export const SWRDemo = () => {
  const { data, isLoading, status, error } = useGetDataSWR();
  console.log({ data });
  return (
    <div>
      <h2>SWR demo</h2>
      {isLoading ? (
        "Loading..."
      ) : status === "error" ? (
        <span>Error: {error}</span>
      ) : (
        <div>
          {data?.map((post) => (
            <div className="post-container">
              <Post key={post.id} {...post} />
            </div>
          ))}
        </div>
      )}
    </div>
  );
};
