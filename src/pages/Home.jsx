import { Link } from "react-router-dom";
export const Home = () => {
  const menuItem = [
    {
      label: "Lazy loading demo",
      url: "/lazy-loading",
    },
    {
      label: "HOC demo",
      url: "/random",
    },
    {
      label: "Key map demo",
      url: "/keymap",
    },
    {
      label: "Promise all demo",
      url: "/promise",
    },
    {
      label: "React query",
      url: "/react-query",
    },
    {
      label: "Suspense demo",
      url: "/suspense",
    },
    {
      label: "Virtualize demo",
      url: "/virtualize",
    },
    {
      label: "Reselect Redux Demo",
      url: "/reselect",
    },
    {
      label: "SWR Demo",
      url: "/swr",
    },
    {
      label: "Throttle Demo",
      url: "/throttle",
    },
    {
      label: "Batching Demo",
      url: "/batching",
    },
  ];
  return (
    <>
      <h2>List demo</h2>
      <ul className="rounded-list">
        {menuItem.map((i) => (
          <li>
            <Link to={i.url}>{i.label}</Link>
          </li>
        ))}
      </ul>
    </>
  );
};
