import { useState, useCallback, useEffect } from "react";
import throttle from "lodash.throttle";
import _ from "lodash";
const sizeToRange = (size) => {
  if (size < 600) {
    return "small";
  } else if (size > 1200) {
    return "large";
  }
  return "medium";
};

export const ThrottleDemo = () => {
  const [range, setRange] = useState(0);
  const [rangeDebounce, setRangeDebounce] = useState(0);
  const [rangeNothing, setRangeNothing] = useState(0);

  const handleWindowResize = throttle(
    () => {
      // Execute some expensive operation
      setRange(window.innerWidth);
    },
    100,
    []
  );

  useEffect(() => {
    window.addEventListener("resize", handleWindowResize);
    return () => {
      window.removeEventListener("resize", handleWindowResize);
    };
  }, [handleWindowResize]);

  const handelScroll = _.debounce((e) => {
    setRangeDebounce(e.target.scrollTop);
  }, 200);

  const handelScrollNothing = (e) => {
    setRangeNothing(e.target.scrollTop);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handelScrollThrottled = useCallback(
    throttle((e) => {
      console.log(e.target.scrollTop);
      setRange(e.target.scrollTop);
    }, 100),
    []
  );

  return (
    <>
      {" "}
      <h2>Throttle & Debounce demo</h2>{" "}
      <div className="area area-1">
        <div className="inside inside-1" onScroll={handelScrollNothing}>
          <div className="content content-2"></div>
          <div className="thing thing-1">Nothing</div>
        </div>
        <div className="count count-1">{rangeNothing}</div>
      </div>
      <div className="area area-1">
        <div className="inside inside-1" onScroll={handelScrollThrottled}>
          <div className="content content-2"></div>
          <div className="thing thing-1">Throttle</div>
        </div>
        <div className="count count-1">{range}</div>
      </div>
      <div className="area area-1">
        <div className="inside inside-1" onScroll={handelScroll}>
          <div className="content content-2"></div>
          <div className="thing thing-1">Debounce</div>
        </div>
        <div className="count count-1">{rangeDebounce}</div>
      </div>
    </>
  );
};
