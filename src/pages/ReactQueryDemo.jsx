import { Button, Row } from "antd";
import { useGetDataByReactQuery } from "../hook/useGetDataByReactQuery";
import { Post } from "./LazyLoadingDemo";

export const ReactQueryDemo = () => {
  const { data, status, error, refetch, isFetching } = useGetDataByReactQuery();
  console.log({ error });
  const handleReload = () => {
    refetch();
  };
  return (
    <>
      <Row justify="space-between" align="middle">
        <h2>React Query Demo</h2>
        <Button onClick={handleReload}>Refetch</Button>
      </Row>
      {isFetching ? (
        "Loading..."
      ) : status === "error" ? (
        <span>Error: {error}</span>
      ) : (
        <div>
          {data?.map((post) => (
            <div className="post-container">
              <Post key={post.id} {...post} />
            </div>
          ))}
        </div>
      )}
    </>
  );
};
