import { useGetDataPromiseAllDemo } from "../hook/useGetDataPromiseAllDemo";
import { Post } from "./LazyLoadingDemo";

export const PromiseAllDemo = () => {
  const { dataPrmFirst, dataPrmSecond } = useGetDataPromiseAllDemo();
  return (
    <>
      <h2>Promise all demo</h2>
      <strong> Promise 1</strong>
      <div className="mb-4">
        <h3>{dataPrmFirst?.name}</h3>
        <p>{dataPrmFirst?.description}</p>
        <strong>👀 {dataPrmFirst?.subscribers_count}</strong>{" "}
        <strong>✨ {dataPrmFirst?.stargazers_count}</strong>{" "}
        <strong>🍴 {dataPrmFirst?.forks_count}</strong>
      </div>
      <strong> Promise 2</strong>
      <div>
        {dataPrmSecond?.map((post) => (
          <div className="post-container">
            <Post key={post.id} {...post} />
          </div>
        ))}
      </div>
    </>
  );
};
