import withLoading from "../HOC/withLoading";
import { Image, Row } from "antd";

const RandomImage = (props) => {
  //use HOC check loading
  return (
    <Row justify="center">
      <Image width={200} src={props.url} alt="" preview={false} />
    </Row>
  );
};

export const RandomImagePage = withLoading(RandomImage);
