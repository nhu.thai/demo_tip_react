import { useState } from "react";

export const BatchingDemo = () => {
  const [count, setCount] = useState(0);
  const [flag, setFlag] = useState(false);

  function handleClick() {
    // setCount((c) => c + 1); // Does not re-render yet
    // setFlag((f) => !f); // Does not re-render yet
    // React will only re-render once at the end (that's batching!)
    setTimeout(() => {
      setCount((c) => c + 1); // Does not re-render yet
      setFlag((f) => !f);
    }, 100);
  }
  console.log("render");
  return (
    <div>
      <button onClick={handleClick}>Next</button>
      <h1 style={{ color: flag ? "blue" : "black" }}>{count}</h1>
    </div>
  );
};
