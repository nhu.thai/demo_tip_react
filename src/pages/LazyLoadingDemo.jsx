import LazyLoad from "react-lazyload";
import { useGetDataLazyLoading } from "../hook/useGetDataLazyLoading";

const Loading = () => (
  <div className="post loading">
    <h5>Loading...</h5>
  </div>
);

export const Post = ({ id, title, body }) => (
  <div className="post">
    <div className="post-body">
      <h4>{title}</h4>
      <p>{body}</p>
    </div>
  </div>
);

export const LazyLoadingDemo = () => {
  const data = useGetDataLazyLoading();
  return (
    <div>
      <h2>LazyLoad Demo</h2>
      <div>
        {data.map((post) => (
          <LazyLoad
            key={post.id}
            placeholder={<Loading />}
            className="post-container"
          >
            <Post key={post.id} {...post} />
          </LazyLoad>
        ))}
      </div>
    </div>
  );
};
