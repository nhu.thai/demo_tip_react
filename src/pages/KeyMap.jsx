export const KeyMap = () => {
  //demo about key in map
  const listData = [
    {
      id: 1,
      address: "abcxyz",
      token: "USB",
      borrowRate: "2344",
      availableLoanAmount: "2876464",
    },
    {
      id: 2,
      address: "abcxyz",
      token: "USDC",
      borrowRate: "2344",
      availableLoanAmount: "2876464",
    },
    {
      id: 3,
      address: "abcxyz",
      token: "ETH",
      borrowRate: "2344",
      availableLoanAmount: "2876464",
    },
  ];
  return (
    <>
      <div>Key mappp</div>
      <ul>
        {listData.map((i) => (
          <div key={i.id}>{i.token}</div>
        ))}
      </ul>
    </>
  );
};
