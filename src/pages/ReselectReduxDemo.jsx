// import { createSelector } from "reselect";
import { createSelector } from "reselect";
export const ReselectReduxDemo = () => {
  const count = createSelector(
    (state) => state,
    (count) => count + 1
  );
  console.log(count(2));
  //reselec có tác dụng chỉ tính toán lại khi đầu vào của chúng thayy đổi
  return (
    <div>
      <h2>Reselect</h2>
      {count(2)}
    </div>
  );
};
