import { Suspense } from "react";
import { useGetDataLazyLoading } from "../hook/useGetDataLazyLoading";
import { Post } from "./LazyLoadingDemo";

export const SuspenseDemo = () => {
  const data = useGetDataLazyLoading();
  return (
    <Suspense fallback={<h1>Loading data...</h1>}>
      {data.map((post) => (
        <div className="post-container">
          <Post key={post.id} {...post} />
        </div>
      ))}
    </Suspense>
  );
};
