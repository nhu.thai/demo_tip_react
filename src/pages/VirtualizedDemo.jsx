import {
  List,
  AutoSizer,
  CellMeasurer,
  CellMeasurerCache,
  Column,
  Table,
} from "react-virtualized";
import { useGetDataByReactQuery } from "../hook/useGetDataByReactQuery";
import { useRef } from "react";
import { Post } from "./LazyLoadingDemo";

export const VirtualizedDemo = () => {
  const { data, isFetching } = useGetDataByReactQuery();
  const cache = useRef(
    new CellMeasurerCache({
      fixedWidth: true,
      defaultHeight: 100,
    })
  );

  return (
    <>
      <h2>Virtualize demo</h2>
      {isFetching || !data?.length ? (
        "Loading..."
      ) : (
        <div style={{ width: "100%", height: "100vh" }}>
          <Table
            width={1000}
            height={300}
            headerHeight={20}
            rowHeight={150}
            rowCount={data.length}
            rowGetter={({ index }) => data[index]}
          >
            <Column
              label="List Post"
              cellRenderer={({ rowData }) => (
                <div className="post-container">
                  <Post key={rowData.id} {...rowData} />
                </div>
              )}
              dataKey="name"
              width={100}
            />
            <Column width={200} label="Description" dataKey="description" />
          </Table>
        </div>
      )}
    </>
  );
};
