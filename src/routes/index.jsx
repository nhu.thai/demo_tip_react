import { Suspense } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { BatchingDemo } from "../pages/BatchingDemo";
import { Home } from "../pages/Home";
import { KeyMap } from "../pages/KeyMap";
import { LazyLoadingDemo } from "../pages/LazyLoadingDemo";
import { PromiseAllDemo } from "../pages/PromiseAllDemo";
import { RandomImagePage } from "../pages/RandomImage";
import { ReactQueryDemo } from "../pages/ReactQueryDemo";
import { ReselectReduxDemo } from "../pages/ReselectReduxDemo";
import { SuspenseDemo } from "../pages/SuspenseDemo";
import { SWRDemo } from "../pages/SWRDemo";
import { ThrottleDemo } from "../pages/ThrottleDemo";
import { VirtualizedDemo } from "../pages/VirtualizedDemo";

export const AppRouter = () => {
  return (
    <Router basename="/">
      <Suspense fallback={<div></div>}>
        <Routes>
          <Route path="/key-map" element={<KeyMap to="/key-map" />} />
          <Route path="/random" element={<RandomImagePage to="/random" />} />
          <Route path="/suspense" element={<SuspenseDemo to="/suspense" />} />
          <Route path="/promise" element={<PromiseAllDemo to="/promise" />} />
          <Route
            path="/react-query"
            element={<ReactQueryDemo to="/react-query" />}
          />
          <Route path="/" element={<Home to="/" />} />
          <Route
            path="/lazy-loading"
            element={<LazyLoadingDemo to="/lazy-loading" />}
          />
          <Route
            path="/virtualize"
            element={<VirtualizedDemo to="/virtualize" />}
          />
          <Route
            path="/reselect"
            element={<ReselectReduxDemo to="/reselect" />}
          />
          <Route path="/swr" element={<SWRDemo to="/swr" />} />
          <Route path="/throttle" element={<ThrottleDemo to="/throttle" />} />
          <Route path="/batching" element={<BatchingDemo to="/batching" />} />
        </Routes>
      </Suspense>
    </Router>
  );
};
