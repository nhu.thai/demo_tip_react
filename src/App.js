// import store from "./redux/store";
// import logo from "./logo.svg";
import { QueryClient, QueryClientProvider } from "react-query";
import "./App.css";
import { AppRouter } from "./routes";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import rootReducer from "./redux/reducers";
const queryClient = new QueryClient();
const store = configureStore({ reducer: rootReducer });
function App() {
  return (
    <Provider store={store}>
      <QueryClientProvider client={queryClient}>
        <div className="App">
          <AppRouter />
        </div>
      </QueryClientProvider>
    </Provider>
  );
}

export default App;
